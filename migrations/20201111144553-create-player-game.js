'use strict'
module.exports = {
  /* eslint-disable */
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('PlayerGames', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      user_id: {
        type: Sequelize.INTEGER,
        references: {
          model: 'Players',
          key: 'id',
        },
        onDelete: 'CASCADE',
      },
      game: {
        type: Sequelize.STRING,
      },
      score: {
        type: Sequelize.INTEGER,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: new Date(),
      },
    })
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('PlayerGames')
  },
}
