/* eslint-disable */
'use strict'
const { Model } = require('sequelize')
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')

module.exports = (sequelize, DataTypes) => {
  class Player extends Model {
    static associate(models) {
      this.hasMany(models.PlayerGame, {
        foreignKey: 'user_id',
        as: 'PlayerGame',
      })
    }

    static #encrypt = (password) => bcrypt.hashSync(password, 10)
    static register = async ({
      first_name,
      last_name,
      username,
      password,
      email,
      profile_picture,
    }) => {
      // nanti ini dipasang IF --> username ga boleh sama
      // kalo sama muncul message tolak pembuatan user JSON file (username has been taken)
      try {
        if (username == null || password == null || email == null)
          return Promise.reject(
            'please fill the requirement: username, email and password'
          )
        const isUniqueUsername = await Player.findOne({ where: { username } })
        const isUniqueEmail = await Player.findOne({ where: { email } })
        if (isUniqueUsername !== null || isUniqueEmail !== null)
          return Promise.reject('username or email already exists')
        // ELSE --> next create user
        const encryptedPassword = this.#encrypt(password)
        Promise.resolve(
          this.create({
            first_name,
            last_name,
            username,
            password: encryptedPassword,
            email,
            profile_picture,
          })
        )
      } catch (err) {
        return Promise.reject(err)
      }
    }
    checkPassword = (password) => bcrypt.compareSync(password, this.password)

    generateToken = () => {
      const payload = {
        id: this.id,
        username: this.username,
      }
      const options = {
        expiresIn: '1h',
      }
      const secret = process.env.ACCESS_TOKEN_SECRET
      const token = jwt.sign(payload, secret, options)
      return token
    }
    //refresh token expiresIn time need to be longer than accessToken expiresIn
    generateRefreshToken = () => {
      const payload = {
        id: this.id,
        username: this.username,
      }
      const options = {
        expiresIn: '24h',
      }
      const secret = process.env.REFRESH_TOKEN_SECRET
      const token = jwt.sign(payload, secret, options)
      return token
    }

    static authenticate = async ({ username, password }) => {
      try {
        const Player = await this.findOne({ where: { username } })
        if (!Player) return Promise.reject('User not found!')
        const isPasswordValid = Player.checkPassword(password)
        if (!isPasswordValid) return Promise.reject('Wrong password')
        return Promise.resolve(Player)
      } catch (err) {
        return Promise.reject(err)
      }
    }
  }
  //endpoint pertama /
  //request reset password, input = email
  //query cari user berdasarkan email kalau ketemu diisi .findone(?)
  //kalau ketemu diisi reset pass token n reset pas expired
  //habis itu kirim email

  //endpoint kedua verifikasi link (GET) check params (if param exists select from user where params = ...)
  //kalau ada responsenya ()
  //flag untuk tanda sudah divalidasi bisa reset pass baru (!dibutuhkan untuk reset pass agar tidak bypass endpoint ketiga)
  //ditambahin tanda can_reset = 1

  //endpoint ketiga /ubah-pass
  //input = email, new pass, confirm new pass
  //query cari user berdasarkan email
  //passwordya diganti query.update(pass_user)
  //tanda can_resetnya = 0 lagi
  //finish

  //sekarang pakai OTP (masih bayar)

  Player.init(
    {
      first_name: DataTypes.STRING,
      last_name: DataTypes.STRING,
      username: DataTypes.STRING,
      password: DataTypes.STRING,
      //reset_pass_token, butuh reset pass expired paling lama 24h (string),
      //reset_pass_expired (timestamps),
      //can_reset (boolean) flag default 0
      email: DataTypes.STRING,
      profile_picture: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: 'Player',
    }
  )
  return Player
}
