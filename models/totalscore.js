'use strict'
const { Model } = require('sequelize')
/* eslint-disable */
module.exports = (sequelize, DataTypes) => {
  class TotalScore extends Model {
    static associate(models) {}
  }
  TotalScore.init(
    {
      user_id: DataTypes.INTEGER,
      username: DataTypes.STRING,
      game: DataTypes.STRING,
      score: DataTypes.INTEGER,
    },
    {
      sequelize,
      modelName: 'TotalScore',
    }
  )
  return TotalScore
}
