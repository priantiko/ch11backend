const Player = require('../models').Player
const TotalScore = require('../models').TotalScore
const token = require('../middleware/verifyRefToken')

function format(player) {
  const { id, username } = player
  return {
    id,
    username,
    accessToken: player.generateToken(),
    refreshToken: player.generateRefreshToken(),
  }
}

module.exports = {
  // eslint-disable-next-line
  register: (req, res, next) => {
    Player.register(req.body)
      .then(() => {
        res.status(200).send({ message: 'Player created' })
      })
      .catch((err) => res.status(400).send({ message: err }))
  },
  // eslint-disable-next-line
  login: (req, res, next) => {
    Player.authenticate(req.body)
      .then((player) => {
        res.status(200).send(format(player))
      })
      .catch((err) => res.status(401).json({ message: err }))
  },

  whoami: (req, res) => {
    const currentPlayer = req.user
    return res.json(currentPlayer)
  },

  refreshToken: async (req, res) => {
    try {
      const { refreshToken } = req.body

      if (!refreshToken) throw err // eslint-disable-line

      const player = await token.verifyRefToken(refreshToken)
      //generate new accessToken and refreshToken
      Player.findByPk(player.id).then((player) => {
        res.json(format(player))
      })
    } catch (err) {
      res.status(400).send({ message: err })
    }
  },

  updatePlayer: async (req, res) => {
    try {
      const findUser = await Player.findByPk(req.user.id)
      if (!findUser) throw err('No player found') // eslint-disable-line
      const updating = await findUser.update({
        first_name: req.body.first_name || player.first_name,
        last_name: req.body.last_name || player.last_name,
        username: req.body.username || player.username,
        // password: req.body.password || player.password,
        email: req.body.email || player.email,
        profile_picture: req.body.profile_picture || player.profile_picture,
      })

      const player = await TotalScore.findOne({
        where: { user_id: req.user.id }, // find player
      })

      await player.update({
        username: req.body.username,
      })

      res.status(200).send(updating)
    } catch (err) {
      res.status(400).send({ message: err })
    }
  },
}
