const PlayerGame = require('../models').PlayerGame
const TotalScore = require('../models').TotalScore

const handleGameResult = async (req, res) => {
  const points = {
    W: 3,
    L: 0,
    D: 1,
  }

  const result = points[req.body.result]

  if (!result && result !== 0)
    res.status(400).json({ error: 'Please use correct input' })

  try {
    await PlayerGame.create({
      user_id: req.user.id,
      game: 'Rock-Paper-Scissors',
      score: result,
    })

    const checkData = await TotalScore.findOne({
      where: { username: req.user.username },
    })

    if (checkData === null) {
      await TotalScore.create({
        user_id: req.user.id,
        username: req.user.username,
        game: 'Rock-Paper-Scissors',
        score: result,
      })
    } else if (checkData) {
      await TotalScore.update(
        {
          score: checkData.score + result,
        },
        {
          where: {
            user_id: req.user.id,
          },
        }
      )
    }

    res.status(200).json({ status: 'OK' })
  } catch (error) {
    res.status(400).json({ error: 'Bad request' })
  }
}

const playerHistory = async (req, res) => {
  try {
    const gameLogs = await PlayerGame.findAll({
      where: { user_id: req.user.id },
    })

    res.status(200).json(gameLogs)
  } catch (error) {
    if (error.name === 'TypeError') {
      res.status(400).json({ error: 'TypeError' })
    } else res.status(400).json({ status: 'Error' })
  }
}

module.exports = { handleGameResult, playerHistory }
