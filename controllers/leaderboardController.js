const TotalScore = require('../models').TotalScore

const handleLeaderboard = async (req, res) => {
  try {
    const data = await TotalScore.findAll({
      order: [['score', 'DESC']], // Sorting, dari tertinggi
      limit: 10, // menampilkan 10 besar
    })

    res.json(data)
  } catch (error) {
    res.json({ error })
  }
}

module.exports = { handleLeaderboard }
