/* eslint-disable */
'use strict'

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert(
      'TotalScores',
      [
        {
          user_id: 1,
          username: 'harrypotter',
          game: 'Rock-Paper-Scissors',
          score: 5,
          createdAt: '11 Nov 2020',
          updatedAt: '15 Nov 2020',
        },
        {
          user_id: 2,
          username: 'ronweasley',
          game: 'Rock-Paper-Scissors',
          score: 17,
          createdAt: '12 Nov 2020',
          updatedAt: '17 Nov 2020',
        },
      ],
      {}
    )
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  },
}
