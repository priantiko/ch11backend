/* eslint-disable */
'use strict'

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert(
      'PlayerGames',
      [
        {
          user_id: 1,
          game: 'Rock-Paper-Scissors',
          score: 0,
          createdAt: '8 Nov 2020',
        },
        {
          user_id: 1,
          game: 'Rock-Paper-Scissors',
          score: 5,
          createdAt: '9 Nov 2020',
        },
        {
          user_id: 1,
          game: 'Rock-Paper-Scissors',
          score: 5,
          createdAt: '10 Nov 2020',
        },
        {
          user_id: 2,
          game: 'Rock-Paper-Scissors',
          score: 12,
          createdAt: '10 Nov 2020',
        },
        {
          user_id: 3,
          game: 'Rock-Paper-Scissors',
          score: 17,
          createdAt: '12 Nov 2020',
        },
      ],
      {}
    )
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  },
}
