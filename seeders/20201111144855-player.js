/* eslint-disable */
'use strict'
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert(
      'Players',
      [
        {
          first_name: 'Harry',
          last_name: 'Potter',
          username: 'harrypotter',
          password:
            '$2y$10$SlaxDjpvwTm3CRc.s99wm.fcQ81T5z0FhxkYy8zz7RBKQFF845tbO',
          email: 'harry@gmail.com',
          profile_picture:
            'https://en.wikipedia.org/wiki/File:Frida_Kahlo,_by_Guillermo_Kahlo.jpg',
          createdAt: '10 Nov 2020',
          updatedAt: '10 Nov 2020',
        },
        {
          first_name: 'Ron',
          last_name: 'Weasley',
          username: 'ronweasley',
          password:
            '$2y$10$SlaxDjpvwTm3CRc.t66mm.fcQ81T5z0FhxkYy8zz7RBKQFF845tbO',
          email: 'ron@gmail.com',
          profile_picture:
            'https://en.wikipedia.org/wiki/File:Frida_Kahlo,_by_Guillermo_Kahlo.jpg',
          createdAt: '10 Nov 2020',
          updatedAt: '10 Nov 2020',
        },
        {
          first_name: 'Hermione',
          last_name: 'Granger',
          username: 'hermione',
          password:
            '$2y$10$SlaxDjpvwTm3CRc.t66mm.fcQ81T5z0FhxkYy8zz7RBKQFF845tbO',
          email: 'hermione@gmail.com',
          profile_picture:
            'https://en.wikipedia.org/wiki/File:Frida_Kahlo,_by_Guillermo_Kahlo.jpg',
          createdAt: '10 Nov 2020',
          updatedAt: '10 Nov 2020',
        },
        {
          first_name: 'Luna',
          last_name: 'Lovegood',
          username: 'luna',
          password: 'luna',
          email: 'luna@gmail.com',
          profile_picture:
            'https://en.wikipedia.org/wiki/File:Frida_Kahlo,_by_Guillermo_Kahlo.jpg',
          createdAt: '10 Nov 2020',
          updatedAt: '10 Nov 2020',
        },
        {
          first_name: 'Ginny',
          last_name: 'Weasley',
          username: 'ginny',
          password: 'ginny',
          email: 'ginny@gmail.com',
          profile_picture:
            'https://en.wikipedia.org/wiki/File:Frida_Kahlo,_by_Guillermo_Kahlo.jpg',
          createdAt: '10 Nov 2020',
          updatedAt: '10 Nov 2020',
        },
      ],
      {}
    )
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  },
}
