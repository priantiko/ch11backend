const jwt = require('jsonwebtoken');
require('dotenv').config();

module.exports = {
    verifyRefToken: (refreshToken) => {
        return new Promise((resolve, reject) => {
            jwt.verify(refreshToken, process.env.REFRESH_TOKEN_SECRET, (err, payload) => {
                if (err) return reject("Expired")
                const player = payload
                resolve(player)
            })
        })
    }
}