const errorLogger = (...params) => {
  console.error(...params)
}

const unknownEndpoint = (req, res) => {
  res.status(404).send({ error: 'unknown endpoint' })
}

const errorHandler = (error, req, res, next) => {
  errorLogger(error.message)

  if (error.name === 'CastError' && error.kind === 'ObjectId') {
    return res.status(400).send({ error: 'malformatted id' })
  } else if (error.name === 'JsonWebTokenError') {
    return res.status(401).send({ error: error.message })
  } else if (error.name === 'ValidationError') {
    return res.status(400).json({ error: error.message })
  } else if (
    error.message === 'NoPermission' ||
    error.message === 'No auth token'
  ) {
    return res
      .status(403)
      .json({ error: 'You are not authorized to access this page' })
  } else {
    return res.json({ error: error.message })
  }
  next(error) // eslint-disable-line
}

module.exports = { unknownEndpoint, errorHandler }
